FROM node:alpine

WORKDIR /usr/src/app

ENV PATH usr/src/app/node_modules/.bin:$PATH

COPY ./config ./config
COPY ./plugins ./plugins
COPY ./src ./src
COPY ./utils ./utils
COPY package*.json .
COPY swagger.yaml .

RUN npm install --silent

EXPOSE 3000
CMD ["npm", "start"]