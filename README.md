## Table of contents

- [Table of contents](#table-of-contents)
- [Introduction](#introduction)
  - [Presentation of Digital Signature Gateway](#presentation-of-digital-signature-gateway)
  - [Context](#context)
  - [Simplified architecture](#simplified-architecture)
- [Installation](#installation)
  - [Local](#local)
- [Rest API](#rest-api)
- [Signature workflow](#signature-workflow)
  - [Description of the signature workflow](#description-of-the-signature-workflow)
    - [1 - Initialising the signature](#1---initialising-the-signature)
    - [2 - Get a status](#2---get-a-status)
    - [3 - Sign documents](#3---sign-documents)
    - [4 - Signature is Done](#4---signature-is-done)
    - [5 - Get a the final VC](#5---get-a-the-final-vc)
  - [DSP Configuration](#dsp-configuration)
  - [CredentialRequestSignature](#credentialrequestsignature)
  - [CredentialResponseSignature](#credentialresponsesignature)
  - [CredentialSignature](#credentialsignature)
- [Plugins](#plugins)
  - [Description of plugins](#description-of-plugins)
  - [Add a new plugin in Digital Signature Gateway](#add-a-new-plugin-in-digital-signature-gateway)
  - [Example of a tree structure](#example-of-a-tree-structure)
  - [Example of plugin](#example-of-plugin)
- [Webhooks](#webhooks)
- [Contralia plugin specification](#contralia-plugin-specification)
  - [Contralia plugin workflow](#contralia-plugin-workflow)
  - [Contralia DSP Configuration](#contralia-dsp-configuration)
  - [Contralia webhooks](#contralia-webhooks)
- [IDnomic Sign Specification](#idnomic-sign-specification)
  - [IDnomic Sign workflow](#idnomic-sign-workflow)
  - [IDnomic Sign DSP Configuration](#idnomic-sign-dsp-configuration)
  - [IDnomic Sign Poll Request](#idnomic-sign-poll-request)
- [Authors and acknowledgment](#authors-and-acknowledgment)
- [License](#license)

## Introduction
  ### Presentation of Digital Signature Gateway
  The Digital Signature Gateway project meets the following need:

  - To connect the Gaia-X ecosystem with the electronic signature solutions on the market today.

  This project enables interaction between a Gaia-x ecosystem and digital signature solutions via API routes. This connector is capable of managing interactions that are as generic as possible: 

  - Ability to sign any type of document (pdf, word, exel, etc.),
  - Adding several documents as appendices
  - Addition of several signatories. 

  The initiation of a new signature path is triggered by a DSC (Digital Signature Consumer). DSCs are Gaia-x participants.
  The architecture of the project has been designed in such a way that other signature services can be added easily, without directly affecting the connector's source code. ([More information on the plugins architecture that has been implemented](#plugins)). An API swagger is available at the GET route '/api/swagger' (http://localhost:3000/api/swagger if the project is run locally).
  
  ### Context
  As part of the Gaia-X project, the aim of this tool is to offer providers a simple solution for using different electronic signature solutions on the market today.

  The Digital Signature Gateway is, in fact, a connector that enables connection to different electronic signature providers. And all with the same information entered by the Gatteway (documents to be signed, signatory information, etc.).

  Via the gateway, a provider can easily change its electronic signature service, or use several of them.

  ### Simplified architecture
  ![SimplifiedArchitecture](/documentation/Simplified-Architecture.png)
## Installation

* Clone the project locally:
  
  ```
  git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/digital-signature-gateway.git
  ```
* Navigate to the project directory:
  
  ```
  cd digital-signature-gateway
  ```
  
  ### Local
* Install the dependencies:
  
  ```
  npm install
  ```
* Specify environment variables and run the application:
  
  ```
  $env:PLUGIN_PASSWORD="MY_PASSWORD"; npm start
  ```
  
  Navigate to [http://localhost:3000/api/swagger](http://localhost:3000/api/swagger) for get the swagger


## Rest API

The Digital Signature Gateway provides the following api:

- {service} : The name of signature service
- {did}     : The name of the Digital Signature Consumer (DSC)

| HTTP Verb | url                                        | input                                                                                                    | output                             | Description                                                                                                                          |
| --------- | ------------------------------------------ | -------------------------------------------------------------------------------------------------------- | ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| HTTP POST | https://{service}/DSC/{did}/signature      | multipart/form-data request with CredentialRequestSignature VC and all documents to include in signature | The CredentialResponseSignature VC | Initiate a new contract (signature)                                                                                                  |
| HTTP GET  | https://{service}/DSC/{did}/signature/{id} | n/a                                                                                                      | The CredentialResponseSignature VC | Get the status of the contract (CredentialResponseSignature VC) by the {id} transaction Id present in CredentialResponseSignature VC |
| HTTP PUT  | https://{service}/DSC/{did}                | The DSPConfiguration VC                                                                                  | Confirmation message               | Create or update a DSP configuration                                                                                                 |
| HTTP GET  | https://{service}/DSC/{did}                | n/a                                                                                                      | The DSPConfiguration VC            | Get a DSP confiruration linked to {service} and {did}                                                                                |
| HTTP POST | https://{service}/DSC/signature/callback   | All data pushed by signature platform                                                                    | HTTP code 200                      | The webhooks of signature platform (participant signed, transaction closed, ...)                                                     |

## Signature workflow

```mermaid
sequenceDiagram
    participant Participant1 as Other Partitipant
    participant DSC as DSC
    participant DigitalSignatureGateway as Digital Signature Gateway 
    participant SignaturePlatform as SignaturePlatform
    DSC->>DigitalSignatureGateway:Request a new signature with CredentialRequestSignature VC
    DigitalSignatureGateway->>SignaturePlatform:Initiate a new transaction (send Documents to sign, add signatories, ..)
    SignaturePlatform->>DigitalSignatureGateway: Send the transaction Id
    SignaturePlatform->>DSC:Send mail to request signature
    SignaturePlatform->>Participant1:Send mail to request signature
    DSC->>SignaturePlatform: Sign Document
    Participant1->>SignaturePlatform: Sign Document
    SignaturePlatform->>DigitalSignatureGateway: Webhooks all participant signed
    DigitalSignatureGateway->>SignaturePlatform: Terminate the transaction with transaction Id
    DigitalSignatureGateway->>DigitalSignatureGateway:Build a CredentialSignature VC and sign it
    DSC->>DigitalSignatureGateway: Get a CredentialSignature VC signed
```

### Description of the signature workflow

#### 1 - Initialising the signature

In the first stage of the workflow, the DSC initiates a new signature by calling the rest api **POST /{service}/DSC/{DID}/signature** from the Digital Signature Gateway, where {service} is the name of the signature service used, and {DID} is the DID of the DSC initiating the new signature. This POST request has a multipart/form-data format and contains several documents:

- [The CredentialRequestSignature VC](#credentialrequestsignature): This file is mandatory, as it describes the signature process 

- All the documents to be attached to the signature (the documents to be signed and the documents to be attached as appendices). 
  **WARNING**: these documents must have the same name as specified in the **gx-signature:documentName** parameter in the **gx-signature:attachments** part of the CredentialRequestSignature VC.

When this API is called, the Digital Signature Gateway will execute the start method of the service plugin (passed in the request). It is this method that will be responsible for making the various calls to the signature platform to initiate the new signature (initiate a new signature, download the documents to be signed, download the attachments, add the signatories, create a signature URL, send an email to the signatories, etc.). When a new signing process is launched, the start method retrieves the transaction number created by the signing platform and stores it in the MongoDB database. Then, still in the start method, the [CredentialResponseSignature VC](#credentialresponsesignature) is constructed and sent to the Digital Signature Gateway, which will call on a VC Issuer to sign this VC. Once the VC has been signed, it is returned in response to the POST request. The [CredentialResponseSignature VC](#credentialresponsesignature) contains the transaction number generated by the signature platform and the signature status of all the participants in the contract.

#### 2 - Get a status

Once the CredentialResponseSignature VC has been obtained in response to the previous POST request, calls can be made to the **GET /{service}/DSC/{DID}/signature/{id}** request to check whether the status of the signatories has changed (if they have performed the signature). This request also contains the 'service' and 'DID' parameters, as well as the 'id' parameter, which corresponds to the transaction number contained in the CredentialResponseSignature VC in response to the previous request. 
The response to this request is a new signed CredentialResponseSignature VC, potentially with a new signatory status.

#### 3 - Sign documents

Once the signature has been initiated, all participants in the signature process will receive an email inviting them to go to the signature platform to sign the document(s). Participants must read the entire document before signing it. Once signed, the participant is redirected to the URL specified in the **gx-signature:redirectParticipantTo** parameter of the CredentialRequestSignature.

#### 4 - Signature is Done

When one or all participants have signed, webhooks from the signing platform are sent to the Digital Signature Gateway. This either updates the status of a participant who has just signed. Or the transaction is closed when all participants have signed. [More information about Webkooks](#webhooks). 

#### 5 - Get a the final VC

When the transaction is closed (all participants have signed the documents), two emails are sent to all signatories. 

The first email contains all the documents signed during the transaction.

The second email contains a unique HTTP link that redirects to the Issuance Credential Protocol (ICP), and enables retrieval of the final signed contract VC. This VC is retrieved in two stages:

- Firstly, when the user clicks on the link, he or she is redirected to a verifiable ICP presentation page. This page contains a presentation QR Code that the user must scan with his wallet in order to present his LegalParticipant VC. This LegalParticipant VC is necessary because the ICP will verify that the user is authorized to retrieve the contract VC.

- Once the user has sent the LegalParticipant VC and has the rights to the contract, he or she is redirected to the ICP's issue page. On this page a new QR Code is displayed, which the user must scan to retrieve the signed final contract VC. Once the scan is complete, the contract VC appears in the user's wallet.


This VC presentation and issuance workflow is described in detail in the [ICP documentation.](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer)

### DSP Configuration

Before it can initiate a new signature, the DSC must create a configuration (Digital Signature Provider) for the signature services it wishes to use. This is done using the :

- **PUT /{service}/DSC/{DID}** :
  This route is used to create or update a configuration.  The parameters to be entered in the route are the signature service ({service}) and the DID of the DSC ({DID}). The DSC configuration must be passed in the body of the request, and must be a VC of type DSPConfiguration in JSON format. (Example of a DSPConfiguration VC below). When this request is made, the Digital Signature Gateway executes the 'addProvider' method of the {service} plugin passed as a parameter. This method is then responsible for storing the configuration in the mongo database. If the request is successful, a confirmation message is returned with http code 200, otherwise http code 400 or 500 is returned.

- **GET /{service}/DSC/{DID}** : 
  This route is used to retrieve the configuration, if any, of the signature service ({service}) based on the DID of the DSC ({DID}). When this request is made, the Digital Signature Gateway executes the 'getProvider' method of the {service} plugin passed as a parameter. This method then retrieves the configuration from the mongo database. If the request is successful, the DSPConfiguration VC of the configuration is returned in response with http code 200. Otherwise http code 400 or 500 is returned.

```javascript
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        {
            "CP": "http://cloudprovider/contractSchema.JSONLD",
            "gx-signature": "http://schemaprovider/gxcontract.JSONLD"
        } 
   ],
   "id": "http://contract.provider.eu/46646",
   "type": [
     "VerifiableCredential",
     "DSPConfiguration"
   ],
   "issuer": "did:web:did.provider.eu",
   "issuanceDate": "2022-06-12T19:38:26.853Z",
   "validUntil" : "2024-06-12T19:38:26.853Z",
   "credentialSubject": {
       "@id": "http://contract.signservice.eu/contract/4646534353566744464646646",
       "type": "DSPConfiguration",
       "DSPConfigurationSealLogo": "",
        .....
    },
   "proof": { }
 }
```

The DSPConfiguration VC contains information that is specific to each signature service. It is therefore up to the DSC to fill in the information needed to use the signature plugin. This VC is stored in the MongoDB database by the signature plugin concerned and not directly by the Digital Signature Gateway.

### CredentialRequestSignature

```javascript
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.gaiax.eu/registry/signature/v1"
    ],
    "id": "http://prodider.eu/contract/9999999",
    "type": ["VerifiableCredential", "CredentialRequestSignature"],
    "issuer": "did:web:proovider.eu",
    "issuanceDate": "2010-01-01T00:00:00Z",
    "credentialSubject": {
       "gx-signature:request": {
            "gx-signature:messageBody": "text of the email",
            "gx-signature:id": "did:web:cloudprovider.eu" ,
            "gx-signature:urlcallback": "https://contracts.provider.eu/contract",
            "gx-signature:expirationDate": "2023-10-01T00:00:00Z",
            "gx-signature:redirectParticipantTo": "https://webcatalog.abc-federation.dev.gaiax.ovh/catalog/",
            "gx-signature:signatureLevel": "Simple"
        },
        "gx-signature:signatories": [
            { 
                "gx-signature:userid": "john.doe@cloudprovider.eu",
                "gx-signature:email": "john.doe@cloudprovider.eu" ,
                "gx-signature:did": "did:ebsi:qujALp4bIDg53ZyUfL",
                "gx-signature:firstname": "John",
                "gx-signature:lastname": "Doe",
                "gx-signature:mobile": "+33646273476"
            },
            { 
                "gx-signature:userid": "alice.doe@cloudprovider.eu",
                "gx-signature:email": "alice.doe@cloudprovider.eu",
                "gx-signature:did": "did:web:did.consummer.eu:users:alice",
                "gx-signature:firstname": "Alice",
                "gx-signature:lastname": "Doe",
                "gx-signature:mobile": "+33646273476"
            }
        ],
        "gx-signature:allowedReaders": [
            { 
                "gx-signature:did": "did:web:cloudprovider.fr:legalentity:legalbot",
                "gx-signature:mobile": "+345664533"
            },
            { 
                "gx-signature:email": "bot_cloudprovider@cloudprovider.eu",
                "gx-signature:did": "did:web:cloudprovider.fr:legalentity:legalbot",
                "gx-signature:mobile": "+345664533" 
            },
            { 
                "gx-signature:email": "alice.@cloudprovider.eu",
                "gx-signature:did": "did:web:did.consummer.eu:users:alice",
                "gx-signature:mobile": "+33456765432"
            }
        ],
        "gx-signature:allowedOwners": [
            { 
                "gx-signature:vcId": "http://vc.consummer.fr/vc/FFEDJIOJDIDEJDI"
            },
            { 
                "gx-signature:vcId": "http://vc.provider.com/vc/556FEFFE4654-4554455454545454"
            },
            { 
                "gx-signature:vcId": "http://vc.thirdparty.com/vc/556535334'454545454"
            }
        ],
        "gx-signature:attachments" : [
            { 
                "gx-signature:documentId": "rgrgg66433546646",
                "gx-signature:documentName": "main-pdf.pdf",
                "gx-signature:name": "mainPdf" ,
                "gx-signature:isToSign": "true" 
            },
            { 
                "gx-signature:documentId": "FGFRGR433546646",
                "gx-signature:documentName": "first-attachment.pdf",
                "gx-signature:name": "attachment1",
                "gx-signature:isToSign": "false"
            }
        ], 
        "gx-signature:signature" : {
            "gx-signature:subject" : {
                "CP:customerId": "554435654",
                "CP:offer": "gold"
            },
            "gx-signature:schemas" : [ 
                { 
                    "gx-signature:schema" : "http://cloudprovider/contractSchema.JSONLD",
                    "gx-signature:acronym" : "CP",
                    "gx-signature:type": "cloudproviderContract"
                } 
            ]
        }
    },
  "proof": { }
}
```

The CredentialRequestSignature VC contains the following information:

- **gx-signature:request**: General information about the signature workflow, such as the expiry date, the redirection URL when a participant has just signed, and the level of the signature.
- **gx-signature:signatories**: All the participants in the signature. The parameters in the example above are mandatory. But other parameters can be added depending on the signature service (e.g. civility).
- **gx-signature:allowedReaders**: Person authorised to view the signature contract. This parameter is optional and depends on the support provided by the signature service.
- **gx-signature:attachments**: The set of documents contained in the signature process. Documents with the **gx-signature:isToSign** parameter set to **true** are the documents to be signed (a signature insert will be added automatically at the end of the document by the signature service). Documents whose **gx-signature:isToSign** parameter is equal to **false** are not documents to be signed and are therefore appended appendix file in the signature.
  **WARNING**: The name of the document in the **gx-signature:documentName** parameter must be identical to the name of the document transmitted in the multipart/form-data request of the **HTTP POST https://{service}/DSC/{did}/signature** when initialising a new signature. This allows the Digital Signature Gateway to link documents.
- **gx-signature:signature** : Additional information for the contract. The data in the **gx-signature:subject** parameter is added to the final CredentialSignature VC when the transaction is completed.
- **gx:allowedOwners++: owners of the document. An ower is a user or a corporation identified by a VC. Issuer will request a VC with the id in the list. eg request a legalParticipat VC Id or a physical participant id... If the owner is able to present this Id, the Wallet is allowed to get the VC. 

### CredentialResponseSignature

```javascript
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        {
            "gx-signature": "https://www.w3.org/2018/credentials/v1"
        }
    ],
    "id": "http://prodider.eu/contract/9999999",
    "type": [
        "VerifiableCredential",
        "CredentialResponseSignature"
    ],
    "issuer": "did:web:dufourstorage.provider.gaia-x.community",
    "credentialSubject": {
        "gx-signature:transactionId": "2c969eb6889d0ad70188d457cc7a6966",
        "gx-signature:status": "OPEN",
        "gx-signature:signaturesStatus": [
            {
                "gx-signature:userid": "john.doe@cloudprovider.eu",
                "gx-signature:status": "WAITING"
            },
            {
                "gx-signature:userid": "alice.@cloudprovider.eu",
                "gx-signature:status": "WAITING"
            }
        ]
    },
    "issuanceDate": "2023-06-19T15:50:13.506330+00:00",
    "proof": { }
}
```

The CredentialResponseSignature VC contains :

- **gx-signature:transactionId**: This is the transaction number of the contract. This Id is used with the **GET https://{service}/DSC/{did}/signature/{id}** route to obtain a new CredentialResponseSignature VC, potentially with the signatory status changed, or to obtain a CredentialSignature VC if the transaction has been completed.
- **gx-signature:status**: The status of the transaction.
- **gx-signature:signaturesStatus**: The userid and status of each signatory (WAITING, SIGNED).

### CredentialSignature

```javascript
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        {
            "CP": "http://cloudprovider/contractSchema.JSONLD",
            "gx-signature": "http://schemaprovider/gxsignature.JSONLD"
        }
    ],
    "@id": "http://prodider.eu/contract/9999999",
    "type": [
        "VerifiableCredential",
        "CredentialSignature"
    ],
    "issuer": "did:web:dufourstorage.provider.gaia-x.community",
    "expirationDate": "2023-10-01T00:00:00Z",
    "credentialSubject": {
        "id": "http://prodider.eu/contract/9999999",
        "type": "CredentialSignature",
        "gx-signature:participants": [
            {
                "gx-signature:did": "did:ebsi:qujALp4bIDg53ZyUfL",
                "gx-signature:email": "john.doe@cloudprovider.eu"
            },
            {
                "gx-signature:did": "did:web:did.consummer.eu:users:alice",
                "gx-signature:email": "alice.doe@cloudprovider.eu"
            }
        ],
        "gx-signature:links": [
            {
                "gx-signature:documentId": "rgrgg66433546646",
                "gx-signature:documentName": "mainPdf",
                "gx-signature:documentOriginalName": "main-pdf.pdf",
                "gx-signature:documentLink": "http://signatureplatform.eu/contract/356543-GFDD-4543455.pdf"
            },
            {
                "gx-signature:documentId": "FGFRGR433546646",
                "gx-signature:documentName": "attachment1",
                "gx-signature:documentOriginalName": "first-attachment.pdf",
                "gx-signature:documentLink": "http://signatureplatform.eu/contract/356543-GFDD-4543455.pdf"
            }
        ],
        "gx-signature:subject": {
            "CP:customerId": "554435654",
            "CP:offer": "gold"
        }
    },
    "issuanceDate": "2023-06-19T15:51:33.808334+00:00",
    "proof": { }
}
```

The CredentialSignature VC contains the following information:

- **gx-signature:participants**: All the participants (with their DID and email) who took part in the signature.
- **gx-signature:links**  All the documents contained in the signature contract. 
  **WARNING**: The **gx-signature:documentLink** parameter contains the link to the document, but this is generated by the signature platform. It will therefore have a different expiry date depending on the signature service used.
- **gx-signature:subject**: Additional information from the CredentialRequestSignature VC.

## Plugins

### Description of plugins

A plugins architecture has been implemented in the Digital Signature Gateway. Plugins are nothing more than the implementation of a signature service. This makes it easy to add signature services without having to modify the Digital Signature Gateway source code. Plugins must be added to a folder bearing the name of the signature service, in the plugins folder. Newly added plugins will be automatically loaded when the Digital Signature Gateway application is restarted. Plugins must be **JavaScript classes** with the **.js** extension and must respect the format described below:

- The name of the class must be the name of the signature service.
- The class **MUST** have a static **name** attribute containing the name of the signature service. This attribute will be used to call the application's rest APIs. So if name is **mysevice**, then the {service} parameter of the rest api will take the value **myservice**. To initialise a new signature, the route will be **POST https://{service}/DSC/{did}/signature**
- The class **MUST** implement the following static methods:
  - **static addProvider(input)** : This method is used to add a new DSP configuration. It is executed when a request is made on the HTTP PUT **https://{service}/DSC/{did}**.
  - **static getProvider(input)** : This method is used to retrieve the configuration of a DSP. It is executed when a request is made on the **GET https://{service}/DSC/{did}**.
  - **static start(input)** : This method is used to initialise a new signature path. It contains an input object parameter containing the DID of the DSC at the origin of the request, the CredentialRequestSignature VC, and all the documents to be included in the signature. This method is executed when a request is made on the **POST https://{service}/DSC/{did}/signature**
  - **static getStatus(input)** : This method retrieves either the CredentialResponseSignature VC or the CredentialSignature VC, depending on the status of the transaction (OPEN, CLOSED). This method is executed when the **GET https://{service}/DSC/{did}/signature/{id}** is called.
  - **static callback(input)**: This method is executed when a webhook of the signature platform takes place. It is used to update the status of the signatories or to update the status of the transaction according to the webhooks sent.

### Add a new plugin in Digital Signature Gateway

To add a new plugin to the application, follow these steps:

- Create a new folder in the plugin folder of the Digital Signature Gateway application.
- Give this folder the name of your signature service (not compulsory, but strongly recommended).
- Inside the folder, create a ***.js** file that complies with the above specifications. This file will be your plugin, and will be loaded automatically when the application is restarted. 
- Within your plugin, you are free to import the modules you want (such as axios or fetch for requests). You can also import JavaScript functions from the application's utils folder. These functions allow you to manipulate the three types of VC used (CredentialRequestSignature, CredentialResponseSignature, CredentialSignature).
- If your plugin becomes too large, you can split it into several files. But BE CAREFUL, as a signature plugin, the application loads all files with the .js extension located directly in your plugin folder. So if you want to add utility files to your plugin, you need to create another subfolder containing these files, and import them into your module. See example of tree structure below

### Example of a tree structure

- Digital Signature Gateway
  - config
    - config.json
  - plugins
    - myservice
      - dgplugin.js
      - model
        - schema-mongo.js
    - otherservice
      - dgplugin.js
  - src
    - ...
  - utils
    - ...
  - package.json
  - package-lock.json

In the tree structure above, the **dgplugin.js** file located in the **myservice** and **otherservice** folders will be loaded as a signature plugin by the application. As for the **schema-mongo.js** file located in the **model** folder of the **myservice** folder, it will not be loaded as a signature plugin by the application because it is located in the **model** folder and not directly in the **myservice** folder. This **schema-mongo.js** file therefore serves as a utility file for the **myservice** plugin, and is therefore imported into the **dgplugin.js** file.

### Example of plugin

```javascript
class Myservice {

    /**
     * MANDATORY
     * The name of signature service. This name will be used to call the rest api.
     * If name name is "myservice", the rest api will be called with "https://.../myservice/...".
     */
    static name = "myservice";


    /**
     * MANDATORY
     * Create or update a DSP configuration
     * @param {*} input : object with did and VC DSPConfiguration
     * @returns The result of the operation in JSON format
     */
    static addProvider(input) { }

    /**
     * MANDATORY
     * Get a DSP configuration
     * @param {*} did : did of the Digital Signature Consumers (DSC)
     * @returns The VC DSPConfiguration
     */
    static getProvider(did) { }

    /**
     * MANDATORY
     * Start a new signature (initiate, add signatories, upload files, createSignUrl, send mail)
     * @param {*} input : object with all datas to start a new signature :
     * - input.did : did of the Digital Signature Consumers (DSC)
     * - input.pdfFile : array of pdf files to sign
     * - input.contractRequest : VC contract request
     * @returns VC credentialResponseSignature
     */
    static start(input) { }

    /**
     * MANDATORY
     * Return the status of a signature in format VC credentialResponseSignature or VC credentialSignature if the signature is done
     * @param {*} input : object with transaction id and did :
     * - input.did : did of the Digital Signature Consumers (DSC)
     * - input.id : id of the transaction
     * @returns VC credentialResponseSignature or VC credentialSignature if the signature is done
     */
    static getStatus(input) { }


    /**
     * MANDATORY
     * Webhook called by Myservice when a signatory sign a document or when the transaction is closed
     * @param {*} input : All query parameters pushed by the callback of the signature service
     * @returns
     */
    static callback(input) { }
}

module.exports = Myservice;
```

## Webhooks

A webhook from the signing service occurs when an event occurs in the signing process. 
These webhooks are called by the signature service. So first the DSC must configure the callback url in the signature service options so that it (the signature service) sends the webhooks to the **HTTP POST https://{service}/DSC/signature/callback.**
When a request is made on this route, the Digital Signature Gateway executes the **callback** function of the {service} plugin, providing it with all the query parameters. If a signature service ever requires a different HTTP method from the current HTTP POST route, then the Digital Signature Gateway will have to be modified to support this new HTTP route method. The same applies if webhooks parameters are transmitted in the request body, for example.

## Contralia plugin specification

### Contralia plugin workflow

```mermaid
sequenceDiagram
    participant Participant1 as Other Partitipant
    participant DSC as DSC
    participant DigitalSignatureGateway as Digital Signature Gateway 
    participant SignaturePlatform as Contralia
    DSC->>DigitalSignatureGateway:Request a new signature with CredentialRequestSignature VC
    DigitalSignatureGateway->>SignaturePlatform:Initiate a new transaction (/Contralia/api/v2/{offerCode}/transactions)
    SignaturePlatform->>DigitalSignatureGateway: Send the transaction Id
    DigitalSignatureGateway->>SignaturePlatform:Upload document to sign (Contralia/api/v2/transactions/{transactionId}/document)
    DigitalSignatureGateway->>SignaturePlatform:Upload attachments (Contralia/api/v2/transactions/{transactionId}/attachment)
    DigitalSignatureGateway->>SignaturePlatform:Add signatory (/Contralia/api/v2/transactions/{transactionId}/signatory)
    DigitalSignatureGateway->>SignaturePlatform:Create sign url (/eDoc/api/document/signUrl)
    DigitalSignatureGateway->>SignaturePlatform:Send mail (/Contralia/api/v2/transactions/{transactionId}/sendMail)
    SignaturePlatform->>DSC:Send mail to request signature
    SignaturePlatform->>Participant1:Send mail to request signature
    DSC->>SignaturePlatform: Sign Document
    SignaturePlatform->>DigitalSignatureGateway:Webhooks signatory 'x' signed
    DigitalSignatureGateway->>DigitalSignatureGateway:Update the status of signatory 'x'
    Participant1->>SignaturePlatform: Sign Document
    SignaturePlatform->>DigitalSignatureGateway: Webhooks all participant signed
    DigitalSignatureGateway->>DigitalSignatureGateway: Update the transaction status
    DigitalSignatureGateway->>SignaturePlatform: Terminate the transaction (/Contralia/api/v2/transactions/{transactionId}/terminate)
    DigitalSignatureGateway->>DigitalSignatureGateway:Build a CredentialSignature VC and sign it
    DSC->>DigitalSignatureGateway: Get a CredentialSignature VC signed
```

### Contralia DSP Configuration

The contralia plugin requires a specific DSP configuration:

- **url**: This is the url of the contralia platform. This parameter may change depending on the contract with contralia (if the provider has its own tenant).
- **proxy** : This is the proxy used (if proxy_enabled = true). This parameter is not currently supported in the current version of the implementation.
- **login**: This is the login for the conralia a account used. The password for this account is passed to the application as an environment variable for security reasons.
- **offerCode**: This is the offer code.
- **organizationalUnitCode**: This is the distributor code for the offer.

Example of a DSP configuration specific to contralia:

```javascript
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        {
            "CP": "http://cloudprovider/contractSchema.JSONLD",
            "gx-signature": "http://schemaprovider/gxcontract.JSONLD"
        } 
   ],
   "id": "http://contract.provider.eu/46646",
   "type": [
     "VerifiableCredential",
     "DSPConfiguration"
   ],
   "issuer": "did:web:did.provider.eu",
   "issuanceDate": "2022-06-12T19:38:26.853Z",
   "validUntil" : "2024-06-12T19:38:26.853Z",
   "credentialSubject": {
       "@id": "http://contract.signservice.eu/contract/4646534353566744464646646",
       "type": "DSPConfiguration",
       "DSPConfigurationSealLogo": "",
       "url": "https://www.contralia.fr:443",
       "proxy": {
            "proxy_enabled": "false",
            "host": "proxy.company.com",
            "port": "8080"
        },
        "login": "gaiax",
        "offerCode": "GAIAX",
        "organizationalUnitCode": "GAIAX-DISTRIB"
    },
   "proof": { }
 }
```

### Contralia webhooks

The Contralia plugin currently implements two webhooks:

- A participant has just signed the document: A **SIGNATURE_STATE_CHANGED** webhook is produced. This webhook contains several parameters, including the transaction identifier and the identifier of the signatory who signed the document. The plugin therefore updates the status of the signatory in the database, so that when the **HTTP GET https://{service}/DSC/{did}/signature/{id}** route is called, it returns the CredentialResponseSignature VC with the new status of the signatory who performed the signature.
- The transaction has just entered the complete state (all the participants have signed the documents): A **TRANSACTION_STATE_CHANGED** webhook is generated. This webhook contains the transaction identifier and its new status. The plugin therefore updates the transaction status in the database, so as to return the CredentialSignature VC when the HTTP **GET https://{service}/DSC/{did}/signature/{id}** route is called.

## IDnomic Sign Specification

Idnomic Sign is a remote electronic signature provided by [Eviden](https://eviden.com/). This section describes the general workflow of using Idnomic Sign.

### IDnomic Sign workflow

As Signature Gateway is an entry point to a set of electronic signature services, the below diagram shows how Idnomic Sign is integrated into the Gateway and how Signers can use their wallet to authenticate with Idnomic Sign and to sign documents. 
Basically, the workflow is realized in 5 steps:

1. Negotiate contract to be signed via a business application, such as Nextcloud
2. Signature Gateway setups a new signature workflow, including contract, signers' emails in the Idnomic Sign platform. The Gateway uses the OIDC password grant type or client credentials grant type to authenticate itself to Idnomic Sign. All the authentication information used by Gateway can be configured via the **https://{service}/DSC/{did}** endpoint. The next section illustrates such a configuration in more details
3. Signers authenticate themself to Idnomic Sign with their SSI Wallet
4. Idnomic Sign takes the information in the Signer Credentials to build X509 Signature Certificate
5. Signers sign the submitted documents
6. Signature Gateway polls Idnomic Sign to track the status of the signature workflow

![Workflow](/documentation/idnomic%20sign/img.png);

### IDnomic Sign DSP Configuration

The authentication information as well as poll request parameters can be set up in the Signature Gateway via a configuration below

```javascript
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        {
            "CP": "http://cloudprovider/contractSchema.JSONLD",
            "gx-signature": "http://schemaprovider/gxcontract.JSONLD"
        }
    ],
    "id": "http://contract.provider.eu/46646",
    "type": [
        "VerifiableCredential",
        "DSPConfiguration"
    ],
    "issuer": "did:web:did.provider.eu",
    "issuanceDate": "2022-06-12T19:38:26.853Z",
    "validUntil": "2024-06-12T19:38:26.853Z",
    "credentialSubject": {
        "@id": "http://contract.signservice.eu/contract/4646534353566744464646646",
        "type": "DSPConfiguration",
        "ids-signature:template": "CCMC_MVP",
        "ids-signature:authentication": {
            "url": "https://bjy-idsign01.dev.idnomic.com/auth/realms/IDsign/protocol/openid-connect/token",
            "client_id": "id_sign",
            "client_secret": "ddf5629b-ec0f-4521-a9fb-7fda4b5663ef",
            "grant_type": "password",
            "username": "signer",
            "password": "{noop}password"
        },
        "ids-signature:response": [
            {
                "response_mode": "poll",
                "response_interval": 2000
            }
        ]
    },
    "proof": {}
}
```

### IDnomic Sign Poll Request

In addition to Webhook, the idnomic plugin also has a poll request feature. That is, once a signature workflow has been started, the Gateway polls Idnomic Sign to track the status of the workflow. The configuration of the poll request can also be done via the configuration endpoint mentioned above. An example of such configuration is:

```javascript
"ids-signature:response": [
            {
                "response_mode": "poll",
                "response_interval": 2000
            }
]
```

## Authors and acknowledgment
GXFS-FR

## License
The Digital Signature Gateway is delivered under the terms of the Apache License Version 2.0.
