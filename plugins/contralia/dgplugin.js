const path = require("path");
const util = require('util');
const axios = require('axios');
const cheerio = require('cheerio');
const { Blob } = require("buffer");
const appRoot = require('app-root-path');
const { MongoClient, ObjectId } = require('mongodb');


const log = require(appRoot + '/utils/log.js');
const mail = require(appRoot + '/utils/mail.js');
const logger = require(appRoot + '/src/logger.js');
const HttpError = require(appRoot + '/src/error.js');
const credentialSignature = require(appRoot + '/utils/credentialSignature');
const credentialResponseSignature = require(appRoot + '/utils/credentialResponseSignature');
const credentialRequestSignature = require(appRoot + '/utils/credentialRequestSignature');




// Build mongo URI with env variable
const dbURI = (typeof process.env.MONGO_INITDB_ROOT_USERNAME != 'undefined') ? `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@${process.env.MONGO_SERVICE_HOST}:${process.env.MONGO_SERVICE_PORT}/` : "mongodb://127.0.0.1:27017";

// nconf for get database in config.json (and url mongo if app run locally)
const nconf = require("nconf");
nconf.argv().env().file({ file: path.join(appRoot.path, '/config/config.json')});
const database = nconf.get("database:database");

class Contralia {
    
    /**
     * MANDATORY
     * The name of signature service. This name will be used to call the rest api.
     * If name name is "myservice", the rest api will be called with "https://.../myservice/...".
     */
    static name = "contralia";


    /**
     * Create or update a DSP configuration
     * @param {*} input : object with did and VC DSPConfiguration
     * @returns The result of the operation in JSON format
     */
    static async addProvider(input) {
      /* Mongo client */
      const client = new MongoClient(dbURI, { useNewUrlParser: true, useUnifiedTopology: true }); 

      try {
        /* New configuration to insert in the database */
        let newConfig = {
          did: input.did,
          type: 'DSPConfiguration',
          configuration: input.configuration
        }

        /* Connect to the database and collection */ 
        await client.connect();
        const db = client.db(database);
        const collection = db.collection(Contralia.name);
      
        /* Check if the configuration already exists */
        const configAlreadyExiste = await collection.findOne({ did: input.did, type: 'DSPConfiguration' });
      
        if (configAlreadyExiste) {
          /* The configuration already exists, update it */
          await collection.updateOne({ did: input.did, type: 'DSPConfiguration' }, { $set: { configuration: input.configuration }});
          logger.info(`Configuration successfully updated for service contralia with did ${input.did}`);
          return { message: 'Configuration successfully updated' };
        } else {
          /* The configuration doesn't exist, insert it */
          await collection.insertOne(newConfig);
          logger.info(`Configuration successfully created for service contralia with did ${input.did}`);
          return { message: 'Configuration successfully created' };
        }
      }
      catch (error) {
        throw(error instanceof HttpError ? error : new HttpError(error, 500));
      }
      finally {
        // Close the connection to the database
        await client.close();
      }
    }


    /**
     * Get a DSP configuration
     * @param {*} did : did of the Digital Signature Consumers (DSC)
     * @returns The VC DSPConfiguration
     */
    static async getProvider(did) {
      /* Mongo client */
      const client = new MongoClient(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });  

      try {
        /* Connect to the database and collection */  
        await client.connect();
        const db = client.db(database);
        const collection = db.collection(Contralia.name);

        /* Check if the configuration already exists */
        const configAlreadyExiste = await collection.findOne({ did: did, type: 'DSPConfiguration' });

        if (configAlreadyExiste) {
          /* the configuration already exists, return it */
          return configAlreadyExiste.configuration;
        } else {
          /* The configuration doesn't exist, return an error */
          throw new HttpError(`The configuration doesn\'t exist`, 404);
        }
      }
      catch (error) {
        throw(error instanceof HttpError ? error : new HttpError(error, 500));
      }
      finally {
        // Close the connection to the database
        await client.close();
      }
    }


    /**
     * Start a new signature (initiate, add signatories, upload files, createSignUrl, send mail)
     * @param {*} input : object with all datas to start a new signature :
     * - input.did : did of the Digital Signature Consumers (DSC)
     * - input.pdfFile : array of pdf files to sign
     * - input.contractRequest : VC contract request
     * @returns VC contract response
     */
    static start(input) {
      return new Promise(async (resolve, reject) => {
        let pdfFiles = input.pdfFile;
        let contractRequest = JSON.parse(input.contractRequest.buffer.toString());
        console.log(`New signature request : ${JSON.stringify(contractRequest)}`);

        // Get the configuration of the service
        let config;
        try {
          config = await Contralia.getProvider(input.did);
        }
        catch (error) {
          reject({ stack: `${__filename} at start/getProvider`, messageToClient: `No configuration found for ${input.did}`, data: error });
        }
        const urlContralia = config.credentialSubject.url;
        const login = config.credentialSubject.login;
        const offerCode = config.credentialSubject.offerCode;
        const organizationalUnitCode = config.credentialSubject.organizationalUnitCode;
        const configHeader = { headers: {'Authorization': "Basic " + Buffer.from(`${login}:${process.env.CONTRALIA_PASSWORD}`, 'ascii').toString('base64')}};

        // Function to call api Contralia Core v2 initiate for initiate a new transaction
        async function initiate(customId, nbSignatories) {
          const initiateForm = new FormData();
          initiateForm.append('offerCode', offerCode);
          initiateForm.append('organizationalUnitCode', organizationalUnitCode);
          initiateForm.append('customRef', customId);
          initiateForm.append('testMode', 'true');
          initiateForm.append('signatoriesCount', nbSignatories);

          let url = `${urlContralia}/Contralia/api/v2/${offerCode}/transactions`;
          return new Promise(async (resolve, reject) => {
            try {
              let response = await axios.post(url, initiateForm, configHeader);
              const $ = cheerio.load(response.data, { xmlMode: true });
              resolve($('transaction').attr('id'));
              log.log(log.LEVEL.log, util.inspect({function: "Contralia-start-initiate", result: response.data}, {depth: 3}));
            } catch (error) {
              reject({ stack: `${__filename} at start/initiate`, data: error });
            }
          });
        }

        // Function to call api Contralia Core v2 upload for upload a file to sign
        async function uploadFileToSign(transactionId, name, file, nbSignatories) {
          // Coordinates for placing the signature inserts on the documents to be signed, depending on the number of signatories. 
          // Contralia does not do this automatically.
          let coordinate = [
            { "x": 50, "y" : 650 }, { "x": 200, "y" : 650 }, { "x": 350, "y" : 650 }, { "x": 500, "y" : 650 },
            { "x": 50, "y" : 500 }, { "x": 200, "y" : 500 }, { "x": 350, "y" : 500 }, { "x": 500, "y" : 500 },
            { "x": 50, "y" : 350 }, { "x": 200, "y" : 350 }, { "x": 350, "y" : 350 }, { "x": 500, "y" : 350 },
            { "x": 50, "y" : 200 }, { "x": 200, "y" : 200 }, { "x": 350, "y" : 200 }, { "x": 500, "y" : 200 },
            { "x": 50, "y" : 50 }, { "x": 200, "y" : 50 }, { "x": 350, "y" : 50 }, { "x": 500, "y" : 50 },
          ];

          // Fiedls parameters generate dynamically in function of the number of signatories
          let fieldsSignatory = '';
          for (let i = 0; i < nbSignatories; i++) {
            fieldsSignatory += '<signatorySignature number="' + (i + 1) + '">'
            fieldsSignatory += '<box x="' + coordinate[i].x + '" y="' + coordinate[i].y + '" page="0"/>'
            fieldsSignatory += '</signatorySignature>'
          }

          let fields = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'
          fields += '<fields xmlns="http://www.contralia.fr/champsPdf">'
          fields += fieldsSignatory
          fields += '</fields>'

          const blob = new Blob([file]);
          const uploadForm = new FormData();
          uploadForm.append('id', transactionId);
          uploadForm.append('name', name);
          uploadForm.append('file', blob);
          uploadForm.append('fields', fields);

       (path.extname(name).slice(1)==="pdf")? uploadForm.append('signatureFormat', 'PADES'): uploadForm.append('signatureFormat', 'CADES');

          let url = `${urlContralia}/Contralia/api/v2/transactions/${transactionId}/document`;
          return new Promise(async (resolve, reject) => {
            try {
              let response = await axios.post(url, uploadForm, configHeader);
              resolve({ "status": 200, "response": response.data });
              log.log(log.LEVEL.log, util.inspect({function: "Contralia-start-UploadFile", result: response.data}, {depth: 3}));
            } catch (error) {
              reject({ stack: `${__filename} at start/uploadFile`, data: error });
            }
          });
        }

        // Function to call api Contralia Core v2 add attachment for upload an attachment
        async function uploadAttachment(transactionId, name, file, extension) {
          const blob = new Blob([file]);
          const uploadForm = new FormData();
          uploadForm.append('transactionId', transactionId);
          uploadForm.append('attachmentId', `${name}.${extension}`);
          uploadForm.append('file', blob);
          uploadForm.append('attachmentType', 'ANNEX');

          let url = `${urlContralia}/Contralia/api/v2/transactions/${transactionId}/attachment`;
          return new Promise(async (resolve, reject) => {
            try {
              let response = await axios.post(url, uploadForm, configHeader);
              resolve({ "status": 200, "response": response.data });
              log.log(log.LEVEL.log, util.inspect({function: "Contralia-start-uploadAttachement", result: response.data}, {depth: 3}));
            } catch (error) {
              reject({ stack: `${__filename} at start/uploadAttachment`, data: error });
            }
          });
        }

        // Function to call api Contralia Core v2 signatory for add a signatory
        async function addSignatory(transactionId, signatory) {        
          const signatoryForm = new FormData();
          signatoryForm.append('id', transactionId);
          signatoryForm.append('firstname', signatory["gx-signature:firstname"]);
          signatoryForm.append('lastname', signatory["gx-signature:lastname"]);
          signatoryForm.append('email', signatory["gx-signature:email"]);
          signatoryForm.append('phone', signatory["gx-signature:mobile"]);
          //signatoryForm.append('signatureLevel', 'SIMPLE_LCP');                   if test mode is false
          
          let url = `${urlContralia}/Contralia/api/v2/transactions/${transactionId}/signatory`;
          return new Promise(async (resolve, reject) => {
            try {
              let response = await axios.post(url, signatoryForm, configHeader);
              const $ = cheerio.load(response.data, { xmlMode: true });
              resolve({ "status": "200", "id": $('signature').attr('id') });
              log.log(log.LEVEL.log, util.inspect({function: "Contralia-start-addSignatory", result: response.data}, {depth: 3}));
            } catch (error) {
              reject({ stack: `${__filename} at start/addSignatory`, data: error });
            }
          });
        }

        // Function to call api eDoc v2 createSignUrl for create the sign url
        async function createSignUrl(transactionId, signatoryId, doneUrl) {
          // Config for the otp code send by sms (max 2 sms, test mode, custom message)
          const configXml = `
            <?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <config>
                <otpDeliveryMode>
                    <deliveryMode mode="SMS" count="2" />
                </otpDeliveryMode>
                <genOtpConfig>
                    <test>true</test>
                    <smsCustomSender>SIGNATURE</smsCustomSender>
                    <smsCustomMessage>Votre code de signature est : {OTP}</smsCustomMessage>
                </genOtpConfig>
            </config>
          `;

          const signUrlForm = new FormData();
          signUrlForm.append('transactionId', transactionId);
          signUrlForm.append('signaturesIds', signatoryId);
          signUrlForm.append('config', configXml);
          signUrlForm.append('doneUrl', doneUrl);
          signUrlForm.append('phoneAndEmailReadOnly', 'true');
          signUrlForm.append('forceReading', 'FULL_READING');
          signUrlForm.append('format', 'json');
          signUrlForm.append('includeAttachments', 'true');

          let url = `${urlContralia}/eDoc/api/document/signUrl`;
          return new Promise(async (resolve, reject) => {
            try {
              let response = await axios.post(url, signUrlForm, configHeader);
              resolve({ "status": "200", "url": response.data.url });
              log.log(log.LEVEL.log, util.inspect({function: "Contralia-start-createSignUrl", result: response.data}, {depth: 3}));
            } catch (error) {
              reject({ stack: `${__filename} at start/createSignUrl`, data: error });
            }
          });
        }

        // Function to call api Contralia Core v2 sendMail for send mail to signatory
        function sendMail(transactionId, signatoryId, urlToSign) {
          // Template of the mail
          const mailSubject = 'Signature de documents - Action requise';
          const mailBody = `
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html>
              <head>    
                <meta name="viewport" content="width=device-width"/>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                <title>Signature de documents - Action requise</title>
              </head>
              <body>
                <p>Hello {firstname} {lastname},</p><br>
                <p>You have been invited to sign important documents. Your participation is required to finalize these formalities.</p><br>
                <p>Please click on the link below to access the electronic signature platform.</p><br><br
                <a href="${urlToSign}">${urlToSign}</a><br><br><br>
                <p>Thank you in advance for your cooperation.</p>
              </body>
            </html>
            `
          const sendMailForm = new FormData();
          sendMailForm.append('id', transactionId);
          sendMailForm.append('signatureId', signatoryId);
          sendMailForm.append('mailSubject', mailSubject);
          sendMailForm.append('mailBody', mailBody);

          let url = `${urlContralia}/Contralia/api/v2/transactions/${transactionId}/sendMail`;
          return new Promise(async (resolve, reject) => {
            try {
              let response = await axios.post(url, sendMailForm, configHeader);
              resolve({ "status": "200", "response": response.data });
              log.log(log.LEVEL.log, util.inspect({function: "Contralia-start-sendMail", result: response.data}, {depth: 3}));
            } catch (error) {
              reject({ stack: `${__filename} at start/sendMail`, data: error });
            }
          });
        }

        
        const client = new MongoClient(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });

        try {
          // Get files to sign and attachments
          let files = credentialRequestSignature.getFiles(contractRequest, pdfFiles);
          let filesToSign = files.filesToSign;
          let attachments = files.attachments;

          if (filesToSign.length == 0) {
            reject({ stack: `${__filename} at start/get files to sign`, data: 'Files to sign is empty', messageToClient:'Files to sign is empty. Please verify tha all files and VC CredentialRequestSignature' });
          }

          // Connect to the database and collection
          await client.connect();
          const db = client.db(database);
          const collection = db.collection(Contralia.name);

          // Create objectId for the parameter "customRef"
          const objectId = new ObjectId();

          // Initiate the transaction
          let nbSignatories = credentialRequestSignature.getNbSignatories(contractRequest);
          let transactionId = await initiate(objectId.toString(), nbSignatories);

          // Upload files to sign
          for (let i = 0; i < filesToSign.length; i++) {
            let uploadResult = await uploadFileToSign(transactionId, filesToSign[i].name, filesToSign[i].file, nbSignatories);
            if (uploadResult.status != '200') {
              reject(uploadResult);
              return;
            }
          }

          // Upload attachments
          for (let i = 0; i < attachments.length; i++) {
            let uploadResult = await uploadAttachment(transactionId, attachments[i].name, attachments[i].file, attachments[i].extension);
            if (uploadResult.status != '200') {
              reject(uploadResult);
              return;
            }
          }

          // Add Signatories
          let signatories = credentialRequestSignature.getSignatories(contractRequest);
          for (let i = 0; i < signatories.length; i++) {
            let resonse = await addSignatory(transactionId, signatories[i]);
            if (resonse.status == '200') {
              signatories[i]["gx-signature:signatoryId"] = resonse.id;
              signatories[i]["gx-signature:status"] = "WAITING";
            }
            else {
              reject(resonse);
              return;
            }
          }

          // Create Sign Url
          let redirectUrl = credentialRequestSignature.getRedirectUrl(contractRequest);
          for (let i = 0; i < signatories.length; i++) {
            let response = await createSignUrl(transactionId, signatories[i]["gx-signature:signatoryId"], redirectUrl);
            if (response.status == '200')
              signatories[i]["gx-signature:signUrl"] = response.url;
            else {
              reject(response);
              return;
            }
          }

          // Send mail
          for (let i = 0; i < signatories.length; i++) {
            let response = await sendMail(transactionId, signatories[i]["gx-signature:signatoryId"], signatories[i]["gx-signature:signUrl"]);
            if (response.status != '200') {
              reject(response);
              return;
            }
          }

          // Create data to the contract
          const vcFields = credentialRequestSignature.getVcFields(contractRequest);
          const date = new Date();
          const did = input.did;
          const data = {
            _id: objectId,
            did: did,
            vcFields: vcFields,
            transactionId: transactionId,
            status: 'OPEN',
            signatories: signatories,
            documents: credentialRequestSignature.getAllDocuments(contractRequest),
            date: date
          }

          // Push all the data in database
          await collection.insertOne(data);

          // Logs
          log.log(log.LEVEL.info, util.inspect({function: "Contralia-start", logs: data}, {depth: 3}));

          // Create the VC credentialResponseSignature
          const status = credentialResponseSignature.buildStatus(transactionId, 'OPEN', signatories);
          resolve(credentialResponseSignature.buildVC(did, status, date));
          log.log(log.LEVEL.log, util.inspect({function: "Contralia-start", status: "Done"}));
        }
        catch (error) {
          reject({ stack: `${__filename} at start/connect to mongodb`, data: error });
        }
        finally {
          await client.close();
        }
      });
    }

    /**
     * Return the status of a signature in format VC credentialResponseSignature or VC credentialSignature if the signature is done
     * @param {*} input : object with transaction id and did :
     * - input.did : did of the Digital Signature Consumers (DSC)
     * - input.id : id of the transaction
     * @returns VC credentialResponseSignature or VC credentialSignature if the signature is done
     */
    static async getStatus(input) {
      /* Mongo client */
      const client = new MongoClient(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });

      try {
        /* Connect to the database and collection */
        await client.connect();
        const db = client.db(database);
        const collection = db.collection(Contralia.name);

        const transactionId = input.id;

        /* Find the transaction in database */
        const result = await collection.find({transactionId: transactionId, did: input.did}).toArray();
        if (result.length == 0) {
          throw new HttpError(`Unknown transaction ID : ${transactionId}`, 404);
        }

        /* Check if the transaction is open or done */
        if (result[0].status == 'OPEN') {
          /* Create the VC credentialResponseSignature */
          let status = credentialResponseSignature.buildStatus(result[0].transactionId, result[0].status, result[0].signatories);
          let vcResponseSignature = credentialResponseSignature.buildVC(input.did, status, result[0].date);
          return { status: "OPEN", data: vcResponseSignature };
        }
        else {
          return { status: "DONE", url: result[0].issuanceUrl };
        }
      }
      catch (error) {
        throw(error instanceof HttpError ? error : new HttpError(error, 500));
      }
      finally {
        await client.close();
      }
    }


    /**
     * Webhook called by Contralia when a signatory sign a document or when the transaction is closed
     * @param {*} input : All query parameters pushed by the callback of the signature service
     * @returns
     */
    static async callback(input) {
      /**
       * Function to call api eDoc v2 createViewUrl
       */
      async function createDocumentLink(config, transactionId) {
        try {
          const documentLinkForm = new FormData();
          documentLinkForm.append("transactionId", transactionId);
          documentLinkForm.append("includeAttachments", "true");

          const url = `${config.urlContralia}/eDoc/api/document/viewUrl`;
          const response = await axios.post(url, documentLinkForm, config.configHeader);
          logger.info("Contralia-getStatus-viewUrl successfully execute", { result: response.data });
          return response.data.url;
        }
        catch (error) {
          throw(error instanceof HttpError ? error : new HttpError(error, 500));
        }
      }

      /**
       * Function to change the status of one signatory (SIGNED or REFUSED)
       */
      async function signatureStateChanged(query) {
        const client = new MongoClient(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });

        try {
          /* Connect to the database and collection */
          await client.connect();
          const db = client.db(database);
          const collection = db.collection(Contralia.name);

          /* Get the signatories */
          const result = await collection.find({ _id: new ObjectId(query.dp_customRef), transactionId: query.dp_transactionId }).toArray();
          const signatories = result[0].signatories;
            
          /* Change the status of the signatory with the signatureId */
          for (const signatory of signatories) {
            if (signatory["gx-signature:signatoryId"] == query.dp_signatureId) {
                signatory["gx-signature:status"] = query.dp_state;
            }
          }

          /* Update the database */
          await collection.updateOne({ _id: new ObjectId(query.dp_customRef), transactionId: query.dp_transactionId }, {$set: { signatories: signatories }});
          return true;
        }
        catch (error) {
          throw(error instanceof HttpError ? error : new HttpError(error, 500));
        }
        finally {
          await client.close();
        }
      }
      

      /**
       * Function to change the status of the transaction (CLOSED, ARCHIVED, ABANDONED)
       * This function terminate the transaction with a call to the api Contralia Core v2 terminate
       */
      async function transactionStateChanged(config, query) {
        const client = new MongoClient(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });

        try {
          /* Connect to the database and collection */
          await client.connect();
          const db = client.db(database);
          const collection = db.collection(Contralia.name);

          /* Update the status in the database */
          await collection.updateOne({ _id: new ObjectId(query.dp_customRef), transactionId: query.dp_transactionId }, {$set: { status: query.dp_state }});

          /* Terminate the transaction with a call to the api Contralia Core v2 terminate */
          const url = `${config.urlContralia}/Contralia/api/v2/transactions/${query.dp_transactionId}/terminate`;
          const terminateForm = new FormData();
          terminateForm.append('id', query.dp_transactionId);
          
          await axios.post(url, terminateForm, config.configHeader);
          logger.info("Contralia-callback-transactionStateChanged-terminate successfully execute");
          return;
        }
        catch (error) {
          throw(error instanceof HttpError ? error : new HttpError(error, 500));
        }
        finally {
          await client.close();
        }
      }

      /**
       * Function to check if all signatories have signed
       */
      async function checkAllParticipantsSigned(query) {
        const client = new MongoClient(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });

        try {
          /* Connect to the database and collection */
          await client.connect();
          const db = client.db(database);
          const collection = db.collection(Contralia.name);

          /* Get the signatories */
          const result = await collection.find({ _id: new ObjectId(query.dp_customRef), transactionId: query.dp_transactionId }).toArray();
          const signatories = result[0].signatories;
            
          /* Check if all signatories have signed */
          for (const signatory of signatories) {
            if (signatory["gx-signature:status"] != "SIGNED") {
                return false
            }
          }

          logger.info('All participants signed');
          return true;
        }
        catch (error) {
          throw(error instanceof HttpError ? error : new HttpError(error, 500));
        }
        finally {
          await client.close();
        }
      }


    
      const client = new MongoClient(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });

      try {
        /* Connect to the database and collection*/
        await client.connect();
        const db = client.db(database);
        const collection = db.collection(Contralia.name);

        /* Get the did by the transactionId */
        const result = await collection.find({ transactionId: input.dp_transactionId }).toArray();
        if (result.length === 0) throw new HttpError(`Unknown transaction ID : ${input.dp_transactionId} in callback`, 404)
        
        /* Get the configuration of the service */
        const config = await Contralia.getProvider(result[0].did);
        const urlContralia = config.credentialSubject.url;
        const login = config.credentialSubject.login;
        const configHeader = { headers: {'Authorization': "Basic " + Buffer.from(`${login}:${process.env.CONTRALIA_PASSWORD}`, 'ascii').toString('base64')}};
        const contraliaConfig = { urlContralia, configHeader };

        switch (input.dp_event) {
          case 'SIGNATURE_STATE_CHANGED':
            if (result[0].status != 'OPEN') return;
            await signatureStateChanged(input);

            if (await checkAllParticipantsSigned(input)) {
              /* Terminate the transaction */
              await transactionStateChanged(contraliaConfig, input);

              /* Get the fiels for the final vc */
              const vcFields = result[0].vcFields;
              let participants = credentialSignature.buildParticipants(result[0].signatories);
              let documents = credentialSignature.buildDocuments(result[0].documents);

              /* Get final all attachments URL to view documents */
              const documentLinkUrl = await createDocumentLink(contraliaConfig, input.dp_transactionId);

              /* Add document view url to object "documents" for VC CredentialSinature */
              for (let i = 0; i < documents.length; i++) {
                documents[i]["gx-signature:documentLink"] = documentLinkUrl;
              }

              /* Build CredentialSubject of the vc CredentialSignature */
              const subjectCredentialSignature = credentialSignature.buildCredentialSubject(participants, documents, vcFields['gx-signature:subject']);

              /* Get issuance URL */
              const issuanceUrl = await credentialSignature.getIssuanceUrl(subjectCredentialSignature);

              /* Send issuance Url to all participants and all signed documents */
              if (issuanceUrl.status === 200) {
                for (let signatory of result[0].signatories) {
                  /* All signed Documents */
                  const mailSubject = 'Summary of your signed contract';
                  const mailBody = `
                    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                   <html>
                    <head> 
                          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                          <title>Summary of your signed contract</title>
                    </head>
                    <body>
                    <p>Hello {firstname} {lastname},</p><br>
                    <p>The signature process is now complete. Please find attached all your signed documents.</p>
                    <p>Thank you for your participation</p>
                    <div>
                    <p>To access all your signed documents, click on the following link: <a href="${documentLinkUrl}" target="_blank">Access Signed Documents</a>.</p>
                    <p>You can download all the files by clicking on the link above.</p>
                    </div>
                   </body>
                   </html>`

                  const sendMailForm = new FormData();
                  sendMailForm.append('id', result[0].transactionId);
                  sendMailForm.append('signatureId', signatory["gx-signature:signatoryId"]);
                  sendMailForm.append('mailSubject', mailSubject);
                  sendMailForm.append('mailBody', mailBody);
                  const url = `${urlContralia}/Contralia/api/v2/transactions/${result[0].transactionId}/sendMail`;
                  await axios.post(url, sendMailForm, configHeader);
                  logger.info("Contralia-callback-sendMail successfully execute");


                  /* Send Issueance URL */
                  const bodyMail = {
                    name: `${signatory["gx-signature:firstname"]} ${signatory["gx-signature:lastname"]}`,
                    email: signatory["gx-signature:email"],
                    Subject: "Issuance of your verifiable credential: CedentialSignature",
                    HTMLPart: `Hello ${signatory["gx-signature:firstname"]} ! <br><br> We are pleased to inform you that the process of signing your contract has been finalized. You are invited to retrieve your verifiable credential 'CredentialSignature' using the following url: <a href=\"${issuanceUrl.url}\">${issuanceUrl.response.url}</a>`
                  };

                  await mail.sendMail(bodyMail);
                }
              }              
            }

            logger.info("Contralia/callback successfully execute");
            return;
          case 'TRANSACTION_STATE_CHANGED':
            return true;
          default:
            logger.error("Event not supported");
            return true;
        }
      } catch (error) {
        throw(error instanceof HttpError ? error : new HttpError(error, 500));
      }
      finally {
        await client.close();
      }
    }
}

module.exports = Contralia;
