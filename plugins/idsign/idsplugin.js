const path = require("path");
const util = require('util');
const axios = require('axios');
const appRoot = require('app-root-path');
const { ObjectId, MongoClient } = require('mongodb');
const https = require('https');
const { v4: uuidv4 } = require('uuid');
const e = require("express");

const log = require(appRoot + '/utils/log.js');
const oidcUtils = require(appRoot + '/utils/oidc');
const credentialResponseSignature = require(appRoot + '/utils/credentialResponseSignature');
const credentialRequestSignature = require(appRoot + '/utils/credentialRequestSignature');
const credentialSignature = require(appRoot + '/utils/credentialSignature');
const mail = require(appRoot + '/utils/mail.js');

const databaseUri = `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@${process.env.MONGO_SERVICE_HOST}:${process.env.MONGO_SERVICE_PORT}/`;
const database = "idsign";

class IDSign {

    /**
     * MANDATORY
     * The name of signature service. This name will be used to call the rest api.
     * If name name is "myservice", the rest api will be called with "https://.../myservice/...".
     */
    static name = "idsign";


    /**
     * MANDATORY
     * Create or update a DSP configuration
     * @param {*} input : object with did and VC DSPConfiguration
     * @returns The result of the operation in JSON format
     */
    static addProvider(input) {
        //Extract the body of the request
        let idSignConfig = {
            did: input.did,
            type: 'DSPConfiguration',
            configuration: input.configuration
        };

        return new Promise(async (resolve, reject) => {
            const client = new MongoClient(databaseUri, { useNewUrlParser: true, useUnifiedTopology: true });
            try {
                /* const db = mongoConnect.getDb();
                 const extingConfiguration = await db.collection('idsign').findOne({ did: input.did, type: 'DSPConfiguration' });*/
                //const client  = new MongoClient( databaseUri, { useNewUrlParser: true, useUnifiedTopology: true });
                await client.connect();
                const db = client.db(database);
                const extingConfiguration = await db.collection('idsign').findOne({ did: input.did, type: 'DSPConfiguration' });
                if (extingConfiguration) {
                    await db.collection('idsign').updateOne({ did: input.did, type: 'DSPConfiguration' }, { $set: { configuration: input.configuration } });
                    resolve({ status: 'DONE', message: 'Configuration successfully updated' });
                } else {
                    log.log(log.LEVEL.log, util.inspect({ location: `${__filename} art IDSIGN/addProvider`, message: `Add/Update a DSP Configuration for ProviderDID: ${input.did}` }, {depth: 3}));
                    await db.collection('idsign').insertOne(idSignConfig);
                    resolve({ status: 'DONE', message: 'Configuration successfully created' });
                }
            } catch (err) {
                log.log(log.LEVEL.error, util.inspect({ location: `${__filename} at IDSIGN/AddProvider`, message: err }, {depth: 3}));
                reject({ stack: `${__filename} at IDSIGN/AddProvider`, message: 'Error: an error occured during the update of the configuration' });
            } finally {

                await client.close();
            }
        });
    }


    /**
     * MANDATORY
     * Get a DSP configuration
     * @param {*} did : did of the Digital Signature Consumers (DSC)
     * @returns The VC DSPConfiguration
     */
    static getProvider(did) {
        const client = new MongoClient(databaseUri, { useNewUrlParser: true, useUnifiedTopology: true });
        return new Promise(async (resolve, reject) => {
            try {
                await client.connect();
                const db = client.db(database);
                const existingConfiguration = await db.collection('idsign').findOne({ did: did, type: 'DSPConfiguration' });
                if (existingConfiguration) {
                    resolve(existingConfiguration.configuration);
                } else {
                    log.log(log.LEVEL.log, 'Error: the configuration does not exist');
                    reject({ status: 'FAIL', message: 'Error: the configuration does not exist' });
                }
            } catch (err) {
                log.log(log.LEVEL.error, util.inspect({ location: `${__filename} at GetProvider`, message: err }, {depth: 3}));
                reject({ status: 'FAILED', stack: `${__filename} at IDSIGN/GetProvider`, message: 'Error: an error occurred during fetching DSPConfiguration ' });
            } finally {
                await client.close();
            }
        });
    }


    /**
     * MANDATORY
     * Return the status of a signature in format VC credentialResponseSignature or VC credentialSignature if the signature is done
     * @param {*} input : object with transaction id and did :
     * - input.did : did of the Digital Signature Consumers (DSC)
     * - input.id : id of the transaction
     * @returns VC credentialResponseSignature or VC credentialSignature if the signature is done
     */
    static start(input) {
        const client = new MongoClient(databaseUri, { useNewUrlParser: true, useUnifiedTopology: true });

        return new Promise(async (resolve, reject) => {
            let pdfFiles = input.pdfFile;
            let providerDid = input.did;
            const contractRequest = JSON.parse(input.contractRequest.buffer.toString());

            let dspConfiguration;
            try {
                await client.connect();
                const db = client.db(database);
                dspConfiguration = await db.collection('idsign').findOne({ did: providerDid, type: 'DSPConfiguration' });
            } catch (err) {
                log.log(log.LEVEL.error, util.inspect({ location: `${__filename} at IDSIGN/Start`, message: err}, {depth: 3}));
                reject({ stack: `${__filename} at start/getProvider`, message: `No configuration found for ${providerDid}`, data: err });
            }


            /* async function getAuthzToken(dspConfiguration) {
                const authConfig = dspConfiguration.credentialSubject['ids-signature:authentication'];
    
                return new Promise(async (resolve, reject) => {
                    try {
                        const access_token = await oidcUtils.getAccessToken(authConfig);
                        authz_token = `Bearer ${access_token}`;
                        resolve(authz_token);
                    } catch (err) {
                        reject(new Error('Failed to authenticate with IDSign'));
                    }
                });
            } */

            async function createNewSignatureWorkFlow(idSignUrl, signingWorkFlowCreationRequest, authzToken) {
                return new Promise(async (resolve, reject) => {
                    try {
                        //const authz_token = await getIDSignAuthzToken(dspConfiguration);
                        const axiosConfig = {
                            headers: { 'Content-Type': 'application/json', 'Authorization': authzToken },
                            httpsAgent: new https.Agent({ rejectUnauthorized: false })
                        }
                        const response = await axios.post(idSignUrl, signingWorkFlowCreationRequest, axiosConfig);
                        const signingWorkFlowId = response.data.name;
                        log.log(log.LEVEL.log, util.inspect({location: `${__filename} at IDSIGN/Start`, message: `Created a workflow with the id: ${signingWorkFlowId}`}, {depth: 3}));
                        resolve(signingWorkFlowId);
                    } catch (err) {
                        log.log(log.LEVEL.error, util.inspect( {location: `${__filename} at IDSIGN/Start-CreatingNewSignatureFlow`, message: err}, {depth: 3}));
                        reject({ stack: `${__filename} at start/createNewSignatureWorkflow`, data: error });
            
                    }
                });
            }

            async function startSignatureWorkFlow(idSignUrl, authzToken) {
                return new Promise(async (resolve, reject) => {
                    try {
                        const axiosConfig = {
                            headers: { 'Content-Type': 'application/json', 'Authorization': authzToken },
                            httpsAgent: new https.Agent({ rejectUnauthorized: false })
                        }
                        const response = await axios.post(idSignUrl, null, axiosConfig);
                        log.log(log.LEVEL.log, util.inspect({location: `${__filename} at IDSIGN/Start-StartSignatureWorkflow`, message: `Started a workflow withid: ${idSignUrl}`, data: response.status}, {depth: 3}));
                        resolve({ status: response.status });
                    } catch (err) {
                        log.log(log.LEVEL.error, util.inspect({ location: `${__filename} at IDSIGN/Start-StartSignatureflow`, message: err}, {depth:3} ));
                        reject({ stack: `${__filename} at start/startSignatureWorkflow`, data: err });
                    }
                });
            }

            // Authenticate Signature Gateway with Keycloak
            const authz_token = await oidcUtils.getIDSignAuthzToken(dspConfiguration.configuration);

            // Extract all the emails of signers:
            const signerMails = [];
            contractRequest.credentialSubject['gx-signature:signatories'].forEach(item => {
                const mailPerNode = [];
                mailPerNode.push(item['gx-signature:email']);
                signerMails.push(mailPerNode);
            });

            //Extract the Signers
            let signers = credentialRequestSignature.getSignatories(contractRequest);
            for (let i = 0; i < signers.length; i++) {
                signers[i]["gx-signature:status"] = "WAITING";
            }

            // Extract all the documents to be signed and convert them into base64-based format
            const toBeSignedPdfFiles = pdfFiles.map((item) => {
                return {
                    "name": item.originalname,
                    "docContent": item.buffer.toString('base64'),
                    "mimeType": item.mimetype
                };
            })

            // Create a new signing flow to the IDSign API
            const alias = `ccmc-idsign-${uuidv4()}`;
            const template = dspConfiguration.configuration.credentialSubject['ids-signature:template'];

            const newWorkFlowRequest = {
                alias: alias,
                template: template,
                actorsPerNode: signerMails,
                documents: toBeSignedPdfFiles
            };

            const idsWorkflowUrl = `https://bjy-idsign01.dev.idnomic.com/idsign/sigOps/workflows`;
            const workflowId = await createNewSignatureWorkFlow(idsWorkflowUrl, newWorkFlowRequest, authz_token);
            log.log(log.LEVEL.log, `Successfully created a workflow with the id: ${workflowId}`);
            

            // Start the signing flow created above
            const workflowStartUrl = `https://bjy-idsign01.dev.idnomic.com/idsign/sigOps/workflows/${workflowId}/start`;
            await startSignatureWorkFlow(workflowStartUrl, authz_token);

            // Insert workflow data in the database: create data to the contract
            const vcFields = credentialRequestSignature.getVcFields(contractRequest);
            const date = new Date();
            const did = input.did;
            const objectId = new ObjectId();
            const data = {
                _id: objectId,
                did: did,
                vcFields: vcFields,
                workflowId: workflowId,
                status: 'RUNNING',
                signers: signers,
                documents: credentialRequestSignature.getAllDocuments(contractRequest),
                date: date
            }
            try {
                const db = client.db(database);
                await db.collection('idsign').insertOne(data);
                // Create the VC credentialResponseSignature
                const status = credentialResponseSignature.buildStatus(workflowId, 'OPEN', signers);
                const query = {
                    id: workflowId,
                    did: did
                };
                IDSign.callback(query);

                resolve(credentialResponseSignature.buildVC(did, status, date));


            } catch (err) {
                log.log(log.LEVEL.err, util.inspect({ location: `${__filename} at IDSIGN/Start`, message: 'Failed to run the start function', data: err}, {depth:3})); 
                reject({ stack: `${__filename} at start`, data: err });
                
            } finally {
                log.log(log.LEVEL.log, `${__filename} at idsign/start: Getting out of the function START inside the plugin`);
                client.close();
            }
        });
    }

    /**
     * Return the status of a signature in format VC credentialResponseSignature or VC credentialSignature if the signature is done
     * @param {*} input : object with transaction id and did :
     * - input.did : did of the Digital Signature Consumers (DSC)
     * - input.id : id of the transaction
     * @returns VC credentialResponseSignature or VC credentialSignature if the signature is done
     */

    static getStatus(input) {

        const client = new MongoClient(databaseUri, { useNewUrlParser: true, useUnifiedTopology: true });

        return new Promise(async (resolve, reject) => {

            try {

                await client.connect();
                const db = client.db(database);

                const workflowId = input.id;
                const providerDid = input.did;
                const workflowData = await db.collection('idsign').findOne({ workflowId: workflowId, did: providerDid });

                console.log('Workflow data is:  ', workflowData);

                if (workflowData.length == 0) {
                    log.log(log.LEVEL.log, `Failed to get details of the workflow ${workflowId}`);
                    reject({ stack: `${__filename} at getStatus`, data: `Failed to get data of the workflow ${workflowId}` });
                }

                if (workflowData.status === 'RUNNING') {
                    log.log(log.LEVEL.log, util.inspect({location: `${__filename} at IDSIGN/getStatus`, message: `Verifying the status of the flow: ${workflowId}`}, {depth: 3} ));
                    // resolve({ status: 'RUNNING', data: 'SignatureCredential to be build' });

                    let status = credentialResponseSignature.buildStatus(workflowData.transactionId, workflowData.status, workflowData.signers);
                    let vcResponseSignature = credentialResponseSignature.buildVC(providerDid, status, workflowData.date);
                    resolve({ status: "OPEN", data: vcResponseSignature });

                } else {
                    log.log(log.LEVEL.log, util.inspect( { location: `${__filename} at idsign/getStatus`, message: `WorkflowID:${workflowId} has finished `}, {depth :3} ));
                    resolve({ status: 'DONE', url: workflowData.issuanceUrl });
                }

            } catch (err) {
                log.log(log.LEVEL.error, util.inspect({location: `${__filename} at IDSIGN/getStatus`, message:err},  {depth:3}));
                reject({ stack: `${__filename} at IDSIGN/getStatus`, message: err});
                
            } finally {
                
                await client.close();
            }
        });
    }


    /**
     * MANDATORY
     * Webhook called by Myservice when a signatory sign a document or when the transaction is closed
     * @param {*} input : All query parameters pushed by the callback of the signature service
     * @returns
     */
    static async callback(query) {
        const workflowId = query.id;
        const providerDid = query.did;
        const idSignCheckStatusUrl = `https://bjy-idsign01.dev.idnomic.com/idsign/sigOps/workflows/${workflowId}`

        const client = new MongoClient(databaseUri, { useNewUrlParser: true, useUnifiedTopology: true });
        return new Promise(async (resolve, reject) => {
            //const db = mongoConnect.getDb();

            await client.connect();
            const db = client.db(database);

            //Get Poll request Configuration
            const dspConfiguration = await db.collection('idsign').findOne({ did: providerDid, type: 'DSPConfiguration' });
            const response_poll_mode = dspConfiguration.configuration.credentialSubject['ids-signature:response'][0];
            const poll_interval = response_poll_mode['response_interval'];
            console.log('Poll interval is: ', poll_interval);

            //Get authorization token
            const authz_token = await oidcUtils.getIDSignAuthzToken(dspConfiguration.configuration);
            const axiosConfig = {
                headers: { 'Content-Type': 'application/json', 'Authorization': authz_token },
                httpsAgent: new https.Agent({ rejectUnauthorized: false })
            }

            try {
                console.log('Workflow ID: ', workflowId);
                const previousWorkflowDesc = await db.collection('idsign').findOne({ workflowId: workflowId });
                const previousWorkFlowStatus = previousWorkflowDesc.status;

                const idsWorkFlowStatusPollStatusRequestID = setInterval(async () => {
                    const client = new MongoClient(databaseUri, { useNewUrlParser: true, useUnifiedTopology: true });
                    try {
                        const response = await axios.get(idSignCheckStatusUrl, axiosConfig);
                        
                        const currentWorkFlowDesc = response.data;
                        const currentWorkflowStatus = currentWorkFlowDesc.status;
                        if (currentWorkflowStatus !== previousWorkFlowStatus) {
                            const db = client.db(database);
                            await db.collection('idsign').updateOne({ workflowId: workflowId }, { $set: { status: currentWorkflowStatus } });
                            //clearInterval(idsWorkFlowStatusPollStatusRequestID);
                        }
                        if (currentWorkflowStatus === 'COMPLETE') {

                            const signers = previousWorkflowDesc.signers;
                            signers.forEach(signer => {
                                signer["gx-signature:status"] = 'SIGNED';
                            });
                            
                            const db = client.db(database);
                            await db.collection('idsign').updateOne({ workflowId: workflowId }, { $set: { signers: signers } });

                            // Get the fields for the final vc
                            const vcFields = previousWorkflowDesc.vcFields;
                            let participants = credentialSignature.buildParticipants(previousWorkflowDesc.signers);
                            let documents = credentialSignature.buildDocuments(previousWorkflowDesc.documents);

                            // Build CredentialSubject of the vc CredentialSignature
                            const subjectCredentialSignature = credentialSignature.buildCredentialSubject(participants, documents, vcFields['gx-signature:subject']);

                            // Get issuance URL
                            const issuanceUrl = await credentialSignature.getIssuanceUrl(subjectCredentialSignature);
                            
                            if(issuanceUrl.status === 200){
                                log.log(log.LEVEL.log, util.inspect( { message: issuanceUrl}, {depth: 3}));
                                await db.collection('idsign').updateOne({ workflowId: workflowId}, { $set: {issuanceUrl: issuanceUrl}} );
                            } else {
                                log.log(log.LEVEL.log, util.inspect( { message: issuanceUrl}, {depth: 3}));
                            }
                            
                            for (let signer of signers) {
                                const bodyMail = {
                                    name: `${signer["gx-signature:firstname"]} ${signer["gx-signature:lastname"]}`,
                                    email: signer["gx-signature:email"],
                                    Subject: "Issuance of your verifiable credential: CedentialSignature",
                                    HTMLPart: `Hello ${signer["gx-signature:firstname"]} ! <br><br> We are pleased to inform you that the process of signing your contract has been finalized. You are invited to retrieve your verifiable credential 'CredentialSignature' using the following url: <a href=\"${issuanceUrl.response.url}\">${issuanceUrl.response.url}</a>`
                                };
                                await mail.sendMail(bodyMail);
                            }
                            clearInterval(idsWorkFlowStatusPollStatusRequestID);
                            resolve(true);
                        
                        }
                    } catch (err) {
                        log.log(log.LEVEL.error, util.inspect( {location: `${__filename} at IDSIGN/Callback`, message: err} , {depth:3}));
                        reject({ stack: `${__filename} at callback`, data: err });
                    } finally {
                        await client.close();
                    }
                }, 1000);

            } catch (err) {
                log.log(log.LEVEL.error, util.inspect({location: `${__filename} at IDSIGN/Callback`, message: err}, {depth:3}));
                reject({stack: `${__filename} at IDSIGN/callback`, data: err});
            } finally {
                await client.close();
            }

        });
    }

}

module.exports = IDSign;