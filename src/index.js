const express = require("express");
const bodyParser = require('body-parser');
const nconf = require("nconf");
const rest = require('./rest.js');
const path = require("path");
const plugin = require ('./plugins.js');
const multer = require('multer');
const appRoot = require('app-root-path');
const log = require(appRoot + '/utils/log.js');
const cors = require('cors');

const logger = require('./logger.js')
const HttpError = require('./error.js');

const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");
const swaggerDocument = YAML.load("./swagger.yaml");

/* For local development */
if (process.env.NODE_ENV !== "production") require("dotenv").config();

const defaut_port = 3000;
nconf.argv().env().file({ file: path.join(appRoot.path, './config/config.json')});


const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


/* API Key */
function verifyApiKey(req, res, next) {  
  if (req.headers['x-api-key'] && req.headers['x-api-key'] === process.env.API_KEY_AUTHORIZED) {
    next();
  } else {
    res.status(401).json({ error: 'Unauthorised: wrong api key' });
  }
}

// Config multer
const storage = multer.memoryStorage();

// Lsit of allowed file types
/*const allowedFileTypes = ['application/pdf', 'application/json', 'image/png','application/*'];
const fileFilter = (req, file, cb) => {
  if (allowedFileTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(new Error(`Only the following files are authorised ${allowedFileTypes}`), false);
  }
};*/
//const upload = multer({ storage: storage, fileFilter: fileFilter });
const upload = multer({ storage: storage });


let server = app.listen(nconf.get("default_port") || defaut_port, () => {
    log.log(log.LEVEL.log, `Server starting on port ${nconf.get('default_port') || defaut_port}`);
    plugin.pluginsLoad(app);
});

process.on('SIGTERM', () => {
    log.log(log.LEVEL.info, 'SIGTERM signal received.');
    client.close();
    server.close();
});


app.use(cors({ origin: '*'}));

/**
 * Swagger
 */
app.use('/api/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

/**
 * Initiate a new digital signature in fonction of the service
 */
app.post('/:service/dsc/:did/signature', verifyApiKey, upload.any(), rest.initiateSignature);

/**
 * Get the status of a digital signature
 */
app.get('/:service/dsc/:did/signature/:id', rest.getContract);

/**
 * Create or update the configuration of a DSP
 */
app.put('/:service/dsc/:did', verifyApiKey, rest.updateConfigDSP);

/**
 * Get the configuration of a DSP
 */
app.get('/:service/dsc/:did', verifyApiKey, rest.getConfigDSP);

/**
 * Callback (webhooks)
 */
app.post('/:service/dsc/signature/callback', rest.callbackSignature);


/* Error */
app.use((error, req, res, next) => {
  logger.error(error.message, { stackTrace: error.stack });
  if (error instanceof HttpError && error.statusCode !== 500) {
    res.status(error.statusCode).json({ error: error.message });
  } else {
    res.status(500).json({ error: 'Internal server error' });
  }
});