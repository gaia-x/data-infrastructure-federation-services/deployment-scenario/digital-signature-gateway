const { createLogger, format, transports } = require("winston");

const logger = createLogger({
    format: format.combine(
        format.timestamp(), // Gardez le timestamp
        format.json() // Conservez le format JSON
    ),
    transports: [new transports.Console({})],
});

// Personnalisez le format du message de journal en dÃ©plaÃ§ant le timestamp au dÃ©but
logger.format = format.combine(
    format.timestamp(), // RÃ©pÃ©tez le timestamp
    format.printf(({ timestamp, level, message, ...rest }) => {
        return JSON.stringify({ timestamp, level, message, ...rest });
    })
);

module.exports = logger;