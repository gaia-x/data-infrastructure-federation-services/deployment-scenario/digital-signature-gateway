// librairie pour executer des plugin n'importe ou dans le code
const fs = require('fs');
const path = require('path');
const appRoot = require('app-root-path');
const log = require(appRoot + '/utils/log.js');

global.PLUGINS = new Map();

const pluginDirectory = path.join(appRoot.path, "/plugins/");

// Load all plugins from the plugin directory
var pluginsLoad = function (app) {
  return new Promise((result, error) => {
  try {
    fs.readdirSync(pluginDirectory).forEach(service => {
      if (fs.statSync(pluginDirectory + service).isDirectory()) {
        fs.readdirSync(pluginDirectory + service).forEach(file => {
          if (path.extname(file) == ".js") {
            const plugin = require(pluginDirectory + service + '/' + file);
            pluginsAdd(plugin.name, plugin);
            log.log(log.LEVEL.log, 'load plugin ' + service + '/' + file);
            result('ok');
          }
        });
      }
    });
  } catch (err) { error (err) }
})};

// Add a plugin to the map
var pluginsAdd = function(codeEvent,pointeurFonction) {
  if (!global.PLUGINS.has(codeEvent)) { global.PLUGINS.set(codeEvent, new Array()) };
  global.PLUGINS.get(codeEvent).push(pointeurFonction);
}

// Execute a plugin
var pluginsExecute = function(service, codeEvent, input, request) {
  return new Promise((result, error) => {
    if (global.PLUGINS.has(service)) {
      try {
        const functionToExecute = global.PLUGINS.get(service)[0][codeEvent];
        return result(functionToExecute(input, request));
      }
      catch (err) { return error (err); }
    }
    else result(input);
})}

module.exports = {pluginsExecute, pluginsLoad};
