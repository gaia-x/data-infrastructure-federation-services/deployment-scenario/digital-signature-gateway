const appRoot = require('app-root-path');
const plugin = require('./plugins');
const path = require("path");
const nconf = require("nconf");
const logger = require('./logger');

nconf.argv().env().file({ file: path.join(appRoot.path, '/config/config.json') });



function isValidJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}


function isCredentialRequest(fileObject) {
  try {
    const jsonString = fileObject.buffer.toString('utf8');
    const jsonData = JSON.parse(jsonString);
    return jsonData["type"].includes("CredentialRequestSignature");
  } catch (e) {
    return false;
  }
}



/**
 * Initiate a new signature in fonction of the service
 */
const initiateSignature = (req, res, next) => {
  let pdfFile = [];
  let jsonFile = [];
  for (const file of req.files) {
    if (file.mimetype === 'application/json' && isCredentialRequest(file)) {
        jsonFile.push(file);
    } else {
      pdfFile.push(file);
    }
  }

  if (jsonFile.length > 1) {
    res.send("Error: only one VC CredentialRequestSignature is allowed.");
    return;
  }
  else if (jsonFile.length == 0) {
    res.send("Error: a VC CredentialRequestSignature is mandatory.");
    return;
  }

  // Execute the 'start' plugin
  plugin.pluginsExecute(req.params.service, 'start', { pdfFile: pdfFile, contractRequest: jsonFile[0], did: req.params.did }, req)
    .then(async result => {
      try {
        res.send(result);
      }
      catch (error) {
        next(error);
      }
    })
    .catch(error => {
      next(error);
    });
}


/**
 * Get the status of a digital signature in form of a VC
 */
const getContract = async (req, res, next) => {
  plugin.pluginsExecute(req.params.service, 'getStatus', { did: req.params.did, id: req.params.id }, req)
    .then(async result => {
      try {
        res.send(result);
      }
      catch (error) {
        next(error);
      }
    })
    .catch(error => {
      next(error);
    });
}


/**
 * Callback for the signature service. 
 * This function is called by the signature service when a event is triggered.
 * Event can be :
 * - a participant has signed the contract
 * - a signature has done
 */
const callbackSignature = async (req, res, next) => {
  console.log("Callback detected : ");
  console.log(req.query);

  /* send code 200 to the signature service */
  res.status(200).json({ message: "true" });

  /* Execute the 'callback' plugin */
  plugin.pluginsExecute(req.params.service, 'callback', req.query, req)
    .then(result => {
      logger.info(result);
    })
    .catch(error => {
      logger.error(error, { stackTrace: error.stack });
    });
}

/**
 * Create or update a DSC configuration
 */
const updateConfigDSP = async (req, res, next) => {
  // Check if the body is a DSPConfiguration
  if (req.body == undefined || !req.body.type.includes("DSPConfiguration")) {
    res.status(400).send("Error: the body must be a DSPConfiguration");
    return;
  } else if (isValidJson(req.body)) {
    res.status(400).send("Error: The JSON DSPConfiguration has the wrong format");
    return;
  }

  // execute the 'addProvider' plugin for add the DSP configuration
  plugin.pluginsExecute(req.params.service, 'addProvider', { did: req.params.did, configuration: req.body }, req)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(400).send(error);
    });
}


/**
 * Get the configuration of a DSP
 */
const getConfigDSP = async (req, res, next) => {
  // execute the 'getProvider' plugin
  plugin.pluginsExecute(req.params.service, 'getProvider', req.params.did, req)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500).send(error);
    });
}


module.exports = {
  initiateSignature,
  getContract,
  callbackSignature,
  updateConfigDSP,
  getConfigDSP
}