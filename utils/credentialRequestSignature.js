// Function to nessesary fields from the VC credentialRequest
function getVcFields(credentialRequest) {
    const vcFields = {
        'id': credentialRequest.id,
        'expirationDate': credentialRequest.credentialSubject['gx-signature:request']['gx-signature:expirationDate'],
        'gx-signature:subject': credentialRequest.credentialSubject['gx-signature:signature']['gx-signature:subject'],
    };
    return vcFields;    
}

// Functions to get redirect url from the VC credentialRequest
function getRedirectUrl(credentialRequest) {
    return credentialRequest.credentialSubject['gx-signature:request']['gx-signature:redirectParticipantTo'];
}

// Functions to get the numbers of signatories from the VC credentialRequest
function getNbSignatories(credentialRequest) {
    return credentialRequest.credentialSubject['gx-signature:signatories'].length;
}

// Functions to get the signatories from the VC credentialRequest
function getSignatories(credentialRequest) {
    return credentialRequest.credentialSubject['gx-signature:signatories'];
}

// Functions to get the all document for the final VC credentialSignature
function getAllDocuments(credentialRequest) {
    return credentialRequest.credentialSubject['gx-signature:attachments'];
}

function getFileExtension(filename) {
    return filename.slice((filename.lastIndexOf('.') - 1 >>> 0) + 2);
}

// Functions to get the files to sign and attachments from the VC credentialRequest
function getFiles(credentialRequest, files) {
    try {
        let filesToSign = [];
        let attachments = [];

        for (const file of credentialRequest.credentialSubject['gx-signature:attachments']) {
            const isToSign = file['gx-signature:isToSign'] === 'true';
            const fileType = {
                name: file['gx-signature:name'],
                originalName: file['gx-signature:documentName'],
                extension: getFileExtension(file['gx-signature:documentName']),
                file: files.find((item) => item.originalname === file['gx-signature:documentName']).buffer
            };
        
            if (isToSign) {
                filesToSign.push(fileType);
            } else {
                attachments.push(fileType);
            }
        }
        
        return { filesToSign, attachments };
    }
    catch (error) {
        console.log("Error at get file : " + error);
        throw new Error(error);
    }
}



module.exports = { 
    getVcFields,
    getNbSignatories, 
    getSignatories, 
    getFiles,
    getRedirectUrl,
    getAllDocuments
};