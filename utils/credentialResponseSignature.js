const { v4: uuidv4 } = require('uuid');

function buildStatus(transactionId, status, signatories) {
    signatoriesStatus = [];
    for (const signatory of signatories) {
        signatoriesStatus.push({
            "gx-signature:userid": signatory["gx-signature:userid"],
            "gx-signature:status": signatory["gx-signature:status"]
        });
    }    

    return {
        "gx-signature:transactionId" : transactionId,
        "gx-signature:status": status,
        "gx-signature:signaturesStatus": signatoriesStatus
    }
}


function buildVC(did, status, date) {
    return {
        "@context": [
            "https://www.w3.org/2018/credentials/v1",
            {
                "gx-signature": "https://www.w3.org/2018/credentials/v1"
            }
        ],
        "id": uuidv4(),
        "type": ["VerifiableCredential", "CredentialResponseSignature"],
        "issuer": did,
        "issuanceDate": date,
        "credentialSubject": status
    }
}

module.exports = { buildVC, buildStatus };