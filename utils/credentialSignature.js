const axios = require('axios');
const HttpError = require('../src/error');
const logger = require('../src/logger');

// Function to build the participants for the VC credentialSignature
function buildParticipants(signatories) {
    participants = [];
    for (const signatory of signatories) {
        participants.push({
            "gx-signature:did": signatory["gx-signature:did"],
            "gx-signature:email": signatory["gx-signature:email"]
        });
    }    

    return participants;
}

// Function to build the documents for the VC credentialSignature
function buildDocuments(attachments) {
    let documents = [];
    let addedDocumentNames = new Set();

    for (const attachment of attachments) {
        const documentId = attachment["gx-signature:documentId"];
        const documentName = attachment["gx-signature:documentName"];
        const signatureName = attachment["gx-signature:name"];

        // Check if the document name and original name combination has already been added
        const isDocumentAdded = addedDocumentNames.has(`${documentName}-${signatureName}`);

        if (!isDocumentAdded) {
            // Add document to table and set
            documents.push({
                "gx-signature:documentId": documentId,
                "gx-signature:documentName": documentName,
                "gx-signature:documentOriginalName": signatureName
            });
            addedDocumentNames.add(`${documentName}-${signatureName}`);
        }
    }

    return documents;
}



async function getIssuanceUrl(credentialSubject) {
    try {
        const icpUrl = `https://icp-credentialissuer.aster-x.demo23.gxfs.fr/gaiax/issuance`;
        const payload = {
            issuance_options: {
                oid4vci_version: "oid4vci_draft13",
                oidc_prefix: "openid-credential-offer://?",
                mode: "pre-authorized_code",
                tx_code_required: false,
                tx_code: "4444",
                tx_code_message: "Please provide the one-time code which was sent via e-mail"
            },
            credentials_data: {
                type: "ContractCredential",
                credentialSubject: credentialSubject
            }
        }

        const response = await axios.post(icpUrl, payload);
        logger.info(`Issuance URL : ${response.data.portalUrl}`)
        return { "status": 200, "url": response.data.portalUrl };
    } catch (error) {
        throw new HttpError(`Error at getIssuanceUrl : ${error}`, 500);
    }
}

// Function to build the VC credentialSignature
function buildCredentialSubject(participants, links, subject) {
    return {
        "gx-signature:participants" :  participants,
        "gx-signature:links" : links,
        ...subject
    };
}


module.exports = { buildCredentialSubject, buildParticipants, buildDocuments, getIssuanceUrl };