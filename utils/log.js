const appRoot = require('app-root-path');

const LEVEL = {critical:0, error:1, log:2, info:3};
const path = require("path");

var nconf = require('nconf');
nconf.argv()
     .env()
     .file({ file: path.join(appRoot.path, '/config/config.json') });

const level_output = nconf.get('log:level_output') || LEVEL.info;

var log = function (securityLevel, message) {
  let color = ""; let type ='';
  switch (securityLevel) {
    case LEVEL.critical:
        type='CRITICAL ';
        color= '\x1b[31m';
      break;
    case LEVEL.error:
        type='ERROR ';
        color='\x1b[31m';
        break;
    case LEVEL.log:
        color = '\x1b[0m';
        type= '';
        break;
    case LEVEL.info:
        color = '\x1b[34m';
        type = '';
        break;
    default:
      color = '\x1b[0m';
      type = 'INFO (NOT PROVIDED)';
      break;
  }
  if (securityLevel <= level_output) {
    console.log(color + ' ' + type + message + '\x1b[0m');
  }
  if (securityLevel <= LEVEL.error) { // si c'est une erreur ou pire on met la stack trace
    console.group();
    console.trace();
    console.groupEnd();
  };
}

module.exports = { log, LEVEL };