const Mailjet = require('node-mailjet');
const logger = require('../src/logger');
const HttpError = require('../src/error');

const mailjet = Mailjet.apiConnect(
    process.env.MJ_APIKEY_PUBLIC,
    process.env.MJ_APIKEY_PRIVATE,
); 


function sendMail(data) {
    const request = mailjet.post('send', { version: 'v3.1' }).request({
        Messages: [
            {
                From: {
                    Email: process.env.MAIL_SENDER
                },
                To: [
                    {
                        Email: data.email,
                        Name: data.name
                    }
                ],
                Subject: data.Subject,
                TextPart: data.TextPart,
                HTMLPart: data.HTMLPart
            }
        ]
    })

    request.then((result) => {
        logger.info(`Successfully send mail at ${data.email}`)
    })
    .catch((error) => {
        throw new HttpError(`Error to send mail : ${error}`, 500);
    });
}


function replacePlaceholders(text, data) {
  const placeholders = Object.keys(data);
  let updatedText = text;

  for (const placeholder of placeholders) {
    const placeholderValue = data[placeholder];
    const regex = new RegExp(`{{${placeholder}}}`, 'g');
    updatedText = updatedText.replace(regex, placeholderValue);
  }

  return updatedText;
}


module.exports = {
    sendMail,
    replacePlaceholders
}