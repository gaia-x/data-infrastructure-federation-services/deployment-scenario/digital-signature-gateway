const axios = require('axios');
const https = require('https');

const buildAuthzRequest = (grantType, authConfig) => {
    switch (grantType) {
        case 'password':
            const authzRequest = {
                'client_id': authConfig.client_id,
                'client_secret': authConfig.client_secret,
                'grant_type': authConfig.grant_type,
                'username': authConfig.username,
                'password': authConfig.password,
                'grant_type': grantType
            }

            return authzRequest;
        default:
            console.log('The specified grant type [' + grantType + '] is not supported');
            return;
    }
}

const getAccessToken = async (authConfig) => {

    const grant_type = authConfig['grant_type'];
    const authzRequest = buildAuthzRequest(grant_type, authConfig);
    console.log('The authz request is:    ', authzRequest);

    return new Promise(async (resolve, reject) => {
        const instance = axios.create({
            httpsAgent: new https.Agent({
                rejectUnauthorized: false
            })
        });

        const configs = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        try {
            const authzResponse = await instance.post(authConfig.url, authzRequest, configs);
            const access_token = authzResponse.data.access_token;
            resolve(access_token);

        } catch (err) {
            reject({ status: 'FAIL', message: 'Cannot get an access token'});
            //throw new Error({ 'message': 'Failed to authenticate with Keycloak' });

        }
    })
}

const getIDSignAuthzToken =  async (dspConfiguration) => {
    const authConfig = dspConfiguration.credentialSubject['ids-signature:authentication'];

    return new Promise(async (resolve, reject) => {
        try {
            const access_token = await getAccessToken(authConfig);
            const authz_token = `Bearer ${access_token}`;
            resolve(authz_token);
        } catch (err) {
            reject(new Error('Failed to authenticate with IDSign'));
        }
    });
}


module.exports = {
    getIDSignAuthzToken
};